-module(mod_hello).
-behavior(gen_mod).
-include("ejabberd.hrl").
-export([
    start/2,
    stop/1
    ]).

start(_Host, _Opt) ->
        ?INFO_MSG("Loading module 'mod_hello' ", []).

stop(_Host) ->
        ok.